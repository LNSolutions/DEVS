# THIS FILE IS GENERATED FROM TEMPLATE
#
# CHECK IF YOU ARE EDITING TEMPLATE BEFORE EDIT

Pod::Spec.new do |s|
  s.name             = "DEVS"
  s.version          = "0.1.1"
  s.summary          = "Swift library for discrete event simulation"
  s.homepage         = "https://gitlab.com/LNSolutions/DEVS"
  s.license          = { :type => "Apache License, Version 2.0", :file => "LICENSE" }
  s.author           = { "Filip Klembara" => "filip@klembara.pro" }
  s.source           = { :git => "https://gitlab.com/LNSolutions/DEVS", :tag => s.version }

  
  s.osx.deployment_target = "10.13"
  s.ios.deployment_target = "11.4"

  s.requires_arc = true
  source_files              = 'DEVS/*.swift',

  s.swift_version = "5.0"

  s.source_files            = source_files
end
