#!/bin/sh

source shell/globals.sh

HELP="0"

PATCH_FLAG="0"
MINOR_FLAG="0"
MAJOR_FLAG="0"
ONLY_VERSION="0"

for i in "$@"; do
    if [ "$i" == "-h" ] || [ "$i" == "--help" ] || [ "$i" == "help" ]; then
        HELP="1"
    elif [ "$i" == "-v" ] || [ "$i" == "--verbose" ]; then
    	VERBOSE="1"
    elif [ "$i" == "patch" ]; then
        PATCH_FLAG="1"
    elif [ "$i" == "minor" ]; then
        MINOR_FLAG="1"
    elif [ "$i" == "major" ]; then
        MAJOR_FLAG="1"
    elif [ "$i" == "-n" ] || [ "$i" == "--show-new-version" ]; then
        ONLY_VERSION="1"
    else
    	error "Unknown argument $i"
    fi
done

if [[ $HELP == 1 ]]; then
    verbose "Printing help"
	echo "Usage: updateVersion.sh [options[, ...]]"
	echo ""
	echo "Updates given version in .version file"
	echo ""
	echo "options:"
    echo "  major                   Increase major number"
    echo "  minor                   Increase minor number"
    echo "  patch                   Increase patch number"
	echo "  --verbose, -v           Increase verbosity of informational output"
    echo "  --show-new-version, -n  Prints new version on output"
    echo "  --help, -h, help        Prints this help"
    verbose "Help printed"
	exit 0
fi

verbose "Checking flags sum, expecting 1"
FLAGS_SUM="$(($PATCH_FLAG+$MINOR_FLAG+$MAJOR_FLAG))"
verbose "\t flags sum is $FLAGS_SUM"

if [ "$FLAGS_SUM" != "1" ]; then
    verbose "incorrect flags sum"
    verbose "\t major $MAJOR_FLAG"
    verbose "\t minor $MINOR_FLAG"
    verbose "\t patch $PATCH_FLAG"
    error "You must specify \'major\', \`minor\` or \'patch\' option but can't use more than one at the same time"
fi

verbose "Flags sum is ok"

VERSION_FILE=".version"
verbose "Using version file \`$VERSION_FILE\`"

inlineLogV "Checking $VERSION_FILE\t"
inlineVerbose "\n\tChecking existence\t"
if [ ! -f "$VERSION_FILE" ]; then
    inlineLogV "\n\t"
    error "There is no file with name $VERSION_FILE"
fi
verbose "exists"

inlineVerbose "\tChecking permissions\t"
if [ ! -r "$VERSION_FILE" ]; then
    inlineLogV "\n\t"
    error "No permissions to read file with name $VERSION_FILE"
fi
verbose "permissions ok"
logVOK "Existance and permissions are ok"

OLD_MAJOR="`cut -d. -f1 "$VERSION_FILE"`"
OLD_MINOR="`cut -d. -f2 "$VERSION_FILE"`"
OLD_PATCH="`cut -d. -f3 "$VERSION_FILE"`"
OLD_VERSION="$OLD_MAJOR.$OLD_MINOR.$OLD_PATCH"

verbose "Old version is $OLD_MAJOR.$OLD_MINOR.$OLD_PATCH"

NEW_PATCH="$OLD_PATCH"

if [ "$PATCH_FLAG" == "1" ]; then
    NEW_PATCH="$(($NEW_PATCH+1))"
    verbose "New patch number is $NEW_PATCH"
fi

NEW_MINOR="$OLD_MINOR"

if [ "$MINOR_FLAG" == "1" ]; then
    NEW_MINOR="$(($NEW_MINOR+1))"
    NEW_PATCH="0"
    verbose "New minor number is $NEW_MINOR"
    verbose "New patch number is $NEW_PATCH"
fi

NEW_MAJOR="$OLD_MAJOR"

if [ "$MAJOR_FLAG" == "1" ]; then
    NEW_MAJOR="$(($NEW_MAJOR+1))"
    NEW_MINOR="0"
    NEW_PATCH="0"
    verbose "New major number is $NEW_MAJOR"
    verbose "New minor number is $NEW_MINOR"
    verbose "New patch number is $NEW_PATCH"
fi

FULL_VERSION="$NEW_MAJOR.$NEW_MINOR.$NEW_PATCH"

if [ "$ONLY_VERSION" == "1" ]; then
    logV "$FULL_VERSION"
    exit 0
else
    logV "New version will be $FULL_VERSION"
fi


inlineLogV "Checking git tags\t"
TAGS="$(git tag | sort -u)"

if [ "$TAGS" != "" ]; then

    inlineVerbose "\n"
    inlineVerbose "\tchecking patch number\t"
    if [ "$PATCH_FLAG" == "1" ] ; then
        TP="$(echo "$TAGS" | grep "^$OLD_MAJOR.$OLD_MINOR.$OLD_PATCH")"
        if [ "$TP" == "" ]; then
            inlineLogV "\n"
            error "You are skipping patch versions! Check \`git tag\`."
        fi
    fi
    verbose "ok"

    inlineVerbose "\tchecking minor number\t"
    if [ "$MINOR_FLAG" == "1" ]; then
        TP="$(echo "$TAGS" | grep "^$OLD_MAJOR.$OLD_MINOR.")"
        if [ "$TP" == "" ]; then
            inlineLogV "\n"
            error "You are skipping minor versions! Check \`git tag\`."
        fi
    fi
    verbose "ok"

    inlineVerbose "\tchecking major number\t"
    if [ "$MAJOR_FLAG" == "1" ]; then
        TP="$(echo "$TAGS" | grep "^$OLD_MAJOR.")"
        if [ "$TP" == "" ]; then
            inlineLogV "\n"
            error "You are skipping major versions! Check \`git tag\`."
        fi
    fi
    verbose "ok"
fi
logVOK "git tags are good"

PLIST="DEVS/Info.plist"
inlineLogV "Updating $PLIST\t\t"

TMP_DIR="newInfo.plist.tmp"
inlineVerbose "\n\tReplacing pattern\t"
cat "$PLIST" | sed -e "s/<string>$OLD_VERSION<\/string>/<string>$FULL_VERSION<\/string>/g" > "$TMP_DIR"
verbose "done"
inlineVerbose "\tUpdating $PLIST\t"
mv "$TMP_DIR" "$PLIST"
verbose "done"
logVOK "Updated"

logV "Updating version file $VERSION_FILE"

echo "$FULL_VERSION" > .version
verbose "\tdone"

FILES="Templates/DEVS.podspec,DEVS.podspec"
verbose "Templates for replacing: $FILES"

verbose "Patterns"
verbose "\t<#VERSION_MAJOR#> -> $NEW_MAJOR"
verbose "\t<#VERSION_MINOR#> -> $NEW_MINOR"
verbose "\t<#VERSION_PATCH#> -> $NEW_PATCH"
verbose "\t<#FULL_VERSION#> -> $FULL_VERSION"

for F in $FILES; do
    TMP="`echo "$F" | cut -d, -f1`"
    DST="`echo "$F" | cut -d, -f2`"
    inlineLogV "Replacing patterns in $TMP > $DST\t"
    cat "$TMP" | sed -e "s/<#VERSION_MAJOR#>/$NEW_MAJOR/g" \
        -e "s/<#VERSION_MINOR#>/$NEW_MINOR/g" \
        -e "s/<#VERSION_PATCH#>/$NEW_PATCH/g" \
        -e "s/<#FULL_VERSION#>/$FULL_VERSION/g" > "$DST"
    logVOK "Replaced"
done
