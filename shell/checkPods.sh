#!/bin/sh

source shell/globals.sh

HELP="0"
COMMIT="0"
for i in "$@"; do
    if [ "$i" == "-h" ] || [ "$i" == "--help" ] || [ "$i" == "help" ]; then
        HELP="1"
    elif [ "$i" == "-v" ] || [ "$i" == "--verbose" ]; then
        VERBOSE="1"
    elif [ "$i" == "-c" ] || [ "$i" == "--commit" ]; then
        COMMIT="1"
    else
        error "Unknown argument $i"
        exit 1
    fi
done

if [[ $HELP == 1 ]]; then
    verbose "Printing help"
    echo "Usage: checkPods.sh [options[, ...]]"
    echo ""
    echo "Checks cocoapods, you must be in release branch!"
    echo ""
    echo "options:"
    echo "  --verbose, -v           Increase verbosity of informational output"
    echo "  --commit, -c            Committs all with default message before push"
    echo "  --help, -h, help        Prints this help"
    verbose "Help printed"
    exit 0
fi

VERSION_FILE=".version"

verbose "Version file is $VERSION_FILE file"

inlineLogV "Checking version file with branch name\t"
VERSION="`cat $VERSION_FILE`"
BRANCH_NAME="`git symbolic-ref --short -q HEAD`"
EXP_BRANCH_NAME2="podspecs/$VERSION"
EXP_BRANCH_NAME1="release/$VERSION"
EXP_BRANCH_NAME3="hotfix/$VERSION"

if [ "$EXP_BRANCH_NAME1" != "$BRANCH_NAME" ] && [ "$EXP_BRANCH_NAME3" != "$BRANCH_NAME" ] && [ "$EXP_BRANCH_NAME2" != "$BRANCH_NAME" ]; then
    inlineLogV "\n\t"
    error "Version determines branch name to $EXP_BRANCH_NAME1, $EXP_BRANCH_NAME3 or $EXP_BRANCH_NAME2 but current branch is $BRANCH_NAME"
fi

logVOK "Release branch version is same as in version file"

inlineLogV "Checking git tags\t"

if [ "`git tag | grep "$VERSION"`" != "" ]; then
    inlineLogV "\n\t"
    error "Tag $VERSION already exists"
fi

logVOK "No tag collisions"


trap ctrl_c INT

function ctrl_c() {
    warning "\nCTRL+C trapped"
    rm -f "$TMP_FILE"
    inlineLogV "Removing tag\t\t"
    git tag -d "$VERSION" &> /dev/null &
    spinner
    git push origin :"$VERSION" &> /dev/null &
    spinner
    logVOK "Removed"
    exit 1
}

if [ "$COMMIT" == "1" ]; then
    inlineLogV "Commiting\t\t"
    if [ "$BRANCH_NAME" != "$EXP_BRANCH_NAME2" ]; then
        error "Can't use default commits in branch $BRANCH_NAME\nCheckout from $BRANCH_NAME to $EXP_BRANCH_NAME2 to allow default commit"
    fi
    git add . &> /dev/null &
    spinner
    git commit -m "default message" &> /dev/null &
    spinner
    logVOK "Commited"
fi

verbose "creating temp tag $VERSION"
git tag "$VERSION"

inlineLogV "pushing commits\t\t"
TMP_FILE=.tmp_check_pods.txt
git push &> "$TMP_FILE" &  
spinner
if [[ "$?" -ne 0 ]]; then
    logV "\n"
    verbose "Can't push, removing tag"
    git tag -d "$VERSION" &> /dev/null
    verbose "`cat "$TMP_FILE"`"
    rm -f "$TMP_FILE"
    error "Can\'t push to remote"
fi
rm -f "$TMP_FILE"
logVOK "Pushed"

inlineLogV "pushing tags\t\t"
git push --tags &> /dev/null &
spinner
logVOK "Pushed"

verbose "clearing pod cache"
echo "1" | pod cache clear SSMessenger &> /dev/null
verbose "clear"

logV "Checking podspec"
if [ "$VERBOSE" == "1" ]; then
    verbose "pod spec lint"
    pod spec lint --verbose
else 
    pod spec lint
fi

if [[ "$?" -ne 0 ]]; then
    red "lint failed"
    inlineLogV "Removing tag\t\t"
    git tag -d "$VERSION" &> /dev/null &
    spinner
    git push origin :"$VERSION" &> /dev/null &
    spinner
    logVOK "Removed"
    error "pod spec lint returns nonzero"
fi

ok "Podspec is ok"

inlineLogV "Removing tag\t\t"
git tag -d "$VERSION" &> /dev/null &
spinner
git push origin :"$VERSION" &> /dev/null &
spinner
logVOK "Removed"
