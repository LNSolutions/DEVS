COFF='\033[0m'		# Text Reset
B='\033[0;34m'      # Blue

VERBOSE="0"

green() {
	local G='\033[0;32m'
	printf "$G$1$COFF\n"
}

red() {
	local R='\033[0;31m'
	printf "$R$1$COFF\n"	
}

error() {
#	if [ "$VERBOSE" == "0" ]; then
#		red "ERR"
#	fi
	red "ERROR: $1"
    if [ "$VERBOSE" != "1" ]; then
      warning "For more information run with -v (verbose)"
    fi
    exit 1
}

ok() {
	green "$1"
}

inlineVerbose() {
    local C='\033[0;36m'
    if [ "$VERBOSE" == "1" ]; then
        printf "$C$1$COFF"
    fi

}

verbose() {
    inlineVerbose "$1\n"
}

warning() {
	local Y='\033[0;33m'
	printf "$Y$1$COFF\n"
}

logVOK() {
	if [ "$VERBOSE" == "1" ]; then
		ok "$1"
	else
		ok "OK"
	fi
}

inlinePrint() {
    printf "$1"
}

inlineLogV() {
    inlinePrint "$1"
}

logV() {
    inlineLogV "$1\n"
}

spinner() {
    local pid=$!
    local delay=0.5
    local spinstr='|/-\'
    while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
        local temp=${spinstr#?}
        printf "[%c]" "$spinstr"
        local spinstr=$temp${spinstr%"$temp"}
        sleep $delay
        printf "\b\b\b"
    done
    printf "   \b\b\b"
	wait $pid
	return "$?"
}

