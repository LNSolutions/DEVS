//
//  Coordinator.swift
//  DEVS
//
//  Created by Filip Klembara on 16/06/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

class Coordinator {

    private typealias Event = (time: DEVSTime, dev: DEV)

    private let subsystems: [HashableDev: Coordinable]
    private let devn: DEVNWrapper
    private var eventList = [Event]()
    private(set) var tl = DEVSTime.infinity
    private(set) var tn = DEVSTime.infinity

    init<DN: DEVN>(devn: DN) {
        self.devn = DefaultDEVNWrapper<DN>(wrapping: devn)
        var sub = [HashableDev: Coordinable]()
        for dev in self.devn.devses {
            sub[HashableDev(dev: dev)] = dev.makeCoordinableSystem()
        }
        self.subsystems = sub
    }

    private func addEvent(_ event: Event) {
        for i in 0..<eventList.count {
            let (_, dev) = eventList[i]
            if dev.id == event.dev.id {
                eventList.remove(at: i)
                break
            }
        }

        var j: Int?
        for i in 0..<eventList.count {
            let e = eventList[i]
            if event.time < e.time {
                j = i
                break
            }
        }

        if let i = j {
            eventList.insert(event, at: i)
        } else {
            eventList.append(event)
        }
    }

    private func outputMessage(y: DEVSEvent, d: DEV, t: DEVSTime) -> DEVSEvent? {
        let ret: DEVSEvent?
        if devn.isInfluencer(d, of: devn), let yn = devn.transform(output: y, of: d, toInputOf: devn) {
            ret = yn
        } else {
            ret = nil
        }

        let r1 = devn.influenced(by: d)
        let r2 = r1.compactMap { din -> (DEV, DEVSEvent)? in
            let r = devn.transform(output: y, of: d, toInputOf: din)
            if let rr = r {
                return (din, rr)
            } else {
                return nil
            }
        }
        r2.forEach { din, e in
            // swiftlint:disable:next force_unwrapping
            let r = subsystems[HashableDev(dev: din)]!
            r.inputMessage(x: e, t: t)
            let dNext = (r.tn, din)
            addEvent(dNext)
        }
        return ret
    }
}

extension Coordinator: Coordinable {

    func initialMessage(t: DEVSTime) {
        subsystems.forEach { $0.1.initialMessage(t: t) }
        eventList = subsystems.map { ($0.value.tn, $0.key.dev) }.sorted { $0.0 < $1.0 }

        // swiftlint:disable:next force_unwrapping
        var l = eventList.first!.time
        for i in 1..<eventList.count {
            precondition(eventList[i].time > l || Double.equal(eventList[i].time, l, precise: devDoubleComparationPrecise))
            l = eventList[i].time
        }
        tl = t
        tn = eventList.first?.time ?? tl
        precondition(tl < tn || Double.equal(tl, tn, precise: devDoubleComparationPrecise))
    }

    func plannedMessage(t: DEVSTime) -> DEVSEvent? {
        precondition(Double.equal(t, tn, precise: devDoubleComparationPrecise), "Sync ERROR")
        let evs = eventList.filter { Double.equal(t, $0.0, precise: devDoubleComparationPrecise) }

        let d: DEV
        if evs.count == 1, let dd = evs.first?.dev {
            d = dd
        } else {
            d = devn.select(from: evs.map { $0.dev })
        }
        eventList.remove(at: eventList.firstIndex { $1.id == d.id }!)
        // swiftlint:disable:next force_unwrapping
        let dStar = subsystems[HashableDev(dev: d)]!
        let yn: DEVSEvent?
        if let y = dStar.plannedMessage(t: t) {
            yn = outputMessage(y: y, d: d, t: t)
        } else {
            yn = nil
        }
        let dNext = (dStar.tn, d)
        addEvent(dNext)
        tl = t
        // swiftlint:disable:next force_unwrapping
        tn = eventList.first!.0
        return yn
    }

    func inputMessage(x: DEVSEvent, t: DEVSTime) {
        print("input \(x) at \(t)")
        precondition(tl <= t && t <= tn, "Sync ERROR")
        let r1 = devn.influenced(by: devn)
        let r2 = r1.compactMap { din -> (DEV, DEVSEvent)? in
            let r = devn.transform(output: x, of: devn, toInputOf: din)
            if let rr = r {
                return (din, rr)
            } else {
                return nil
            }
        }
        r2.forEach {
            // swiftlint:disable:next force_unwrapping
            let d = subsystems[HashableDev(dev: $0.0)]!
            d.inputMessage(x: $0.1, t: t)
            let dNext = (d.tn, $0.0)
            addEvent(dNext)
        }
        tl = t
        // swiftlint:disable:next force_unwrapping
        tn = eventList.first!.0
        assert(tl < tn || Double.equal(tl, tn, precise: devDoubleComparationPrecise))
    }
}
