//
//  Simulator.swift
//  DEVS
//
//  Created by Filip Klembara on 16/06/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

class Simulator<D: DEVS> {
    private let devs: D
    private(set) var tl: DEVSTime
    private(set) var tn: DEVSTime
    private var s: D.S

    init(devs: D) {
        self.devs = devs
        tl = Double.infinity
        s = devs.initialState
        tn = Double.infinity
        self.devs.currentStateClosure = { [unowned self] in
            self.s
        }
    }

    deinit {
        devs.currentStateClosure = nil
    }
}

extension Simulator: Coordinable {
    func initialMessage(t: DEVSTime) {
        tl = t
        tn = t + devs.ta(state: s)
        s = devs.initialState
        precondition(tl < tn || Double.equal(tl, tn, precise: devDoubleComparationPrecise))
    }

    func inputMessage(x: DEVSEvent, t: DEVSTime) {
        print(tl, t, tn)
        precondition(tl <= t && t <= tn, "Sync ERROR")
        // swiftlint:disable:next force_cast
        let inx = x as! D.X
        let e = t - tl
        s = devs.deltaExt(state: s, e: e, x: inx)
        tl = t
        tn = tl + devs.ta(state: s)
        precondition(tl < tn || Double.equal(tl, tn, precise: devDoubleComparationPrecise))
    }

    func plannedMessage(t: DEVSTime) -> DEVSEvent? {
        precondition(Double.equal(t, tn, precise: 6), "Sync ERROR")
        let y = devs.lambda(state: s)
        s = devs.deltaInt(state: s)
        tl = t
        tn = tl + devs.ta(state: s)
        precondition(tl < tn || Double.equal(tl, tn, precise: devDoubleComparationPrecise))
        return y
    }
}
