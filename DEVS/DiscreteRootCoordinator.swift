//
//  DiscreteRootCoordinator.swift
//  DEVS
//
//  Created by Filip Klembara on 30/07/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

/// Root coordinator using discrete time
public class DiscreteRootCoordinator<X: DEVSEvent> {
    private let system: Coordinable
    private let output: (DEVSEvent) -> Void
    /// Flag is simulation is running
    public private(set) var isRunning = false
    private let mutex = DispatchSemaphore(value: 1)

    /// Init coordinator
    ///
    /// - Parameters:
    ///   - model: DEV model
    ///   - output: Output function
    ///   - y: Output
    public init<T: DEVSEvent>(model: DEV, output: @escaping (_ y: T) -> Void) {
        system = model.makeCoordinableSystem()
        self.output = { event in
            // swiftlint:disable:next force_cast
            let e = event as! T
            output(e)
        }
    }

    /// Current time
    public var currentTime: Double {
        return t
    }

    private var t = Double.infinity

    private func plannedMessage() {
        if let event = system.plannedMessage(t: t) {
            output(event)
        }
    }

    /// Start simulation with given time
    ///
    /// - Parameter initialTime: initial time (default 0)
    public func startSimulation(initialTime: DEVSTime = 0) {
        isRunning = true
        t = initialTime
        mutex.wait()
        system.initialMessage(t: t)
        mutex.signal()
        while isRunning {
            mutex.wait()
            defer {
                mutex.signal()
            }
            t = system.tn
            repeat {
                plannedMessage()
            } while t == system.tn
        }
    }

    /// Stop simulation
    public func stopSimulation() {
        isRunning = false
    }

    /// Input message
    ///
    /// - Parameter x: Message
    public func inputMessage(x: X) {
        mutex.wait()
        defer {
            mutex.signal()
        }
        guard isRunning else {
            return
        }

        system.inputMessage(x: x, t: t)
    }
}
