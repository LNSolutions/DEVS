//
//  DEVN.swift
//  DEVS
//
//  Created by Filip Klembara on 16/06/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import Foundation

protocol DEVNWrapper: DEV {
    var devses: [DEV] { get }

    func isInfluencer(_: DEV, of: DEV) -> Bool
    func transform(output: DEVSEvent, of: DEV, toInputOf: DEV) -> DEVSEvent?
    func influenced(by: DEV) -> [DEV]
    func select(from: [DEV]) -> DEV
}

/// DEVN
public protocol DEVN: DEV {
    /// Input
    associatedtype X: DEVSEvent
    /// Output
    associatedtype Y: DEVSEvent

    /// Connections
    var connections: [DEVNConnection] { get }

    /// Select function
    ///
    /// - Parameter from: devses
    /// - Returns: Selected DEV
    func select(from: [DEV]) -> DEV
}

/// MARK: - DEVN+Connections
public extension DEVN {
    /// Creates connection between input and given DEVS
    ///
    /// - Parameters:
    ///   - devs: DEVS
    ///   - transformation: Transforming function
    /// - Returns: New connection
    func connectIn<D: DEVS>(toDEVS devs: D, transformation: @escaping (X) -> D.X?) -> DEVNConnection {
        return Connection.InDevs<Self, D>(to: devs, transformation: transformation)
    }
    /// Creates connection between input and given DEVN
    ///
    /// - Parameters:
    ///   - devn: DEVN
    ///   - transformation: Transforming function
    /// - Returns: New connection
    func connectIn<D: DEVN>(toDEVN devn: D, transformation: @escaping (X) -> D.X?) -> DEVNConnection {
        return Connection.InDevn<Self, D>(to: devn, transformation: transformation)
    }
    /// Creates connection between given DEVS and output
    ///
    /// - Parameters:
    ///   - devs: DEVS
    ///   - transformation: Transforming function
    /// - Returns: New connection
    func connectDevsToOut<I: DEVS>(devs: I, transformation: @escaping (I.Y) -> Y?) -> DEVNConnection {
        return Connection.OutDevs<I, Self>(from: devs, transformation: transformation)
    }
    /// Creates connection between given DEVN output
    ///
    /// - Parameters:
    ///   - devn: DEVN
    ///   - transformation: Transforming function
    /// - Returns: New connection
    func connectDevnToOut<I: DEVN>(devn: I, transformation: @escaping (I.Y) -> Y?) -> DEVNConnection {
        return Connection.OutDevn<I, Self>(from: devn, transformation: transformation)
    }

    /// Create connection between input and given DEVS
    ///
    /// - Parameter devs: DEVS
    /// - Returns: New connection
    func connectIn<D: DEVS>(toDEVS devs: D) -> DEVNConnection where X == D.X {
        return Connection.InDevs<Self, D>(to: devs) { $0 }
    }
    /// Create connection between input and given DEVN
    ///
    /// - Parameter devn: DEVN
    /// - Returns: New connection
    func connectIn<D: DEVN>(toDEVN devn: D) -> DEVNConnection where X == D.X {
        return Connection.InDevn<Self, D>(to: devn) { $0 }
    }
    /// Create connection between given DEVS and output
    ///
    /// - Parameter devs: DEVS
    /// - Returns: New connection
    func connectDevsToOut<I: DEVS>(devs: I) -> DEVNConnection where I.Y == Y {
        return Connection.OutDevs<I, Self>(from: devs) { $0 }
    }
    /// Create connection between given DEVN and output
    ///
    /// - Parameter devn: DEVN
    /// - Returns: New connection
    func connectDevnToOut<I: DEVN>(devn: I) -> DEVNConnection where I.Y == Y {
        return Connection.OutDevn<I, Self>(from: devn) { $0 }
    }

    /// Return DEV contained in `devses` from `priority` with lowest index
    ///
    /// - Parameters:
    ///   - devses: DEVSES to pick
    ///   - priority: Priority list
    /// - Returns: Picked DEV
    func prioritized(devses: [DEV], priority: [DEV]) -> DEV {
        for d in priority {
            if devses.contains(where: { $0.id == d.id }) {
                return d
            }
        }
        preconditionFailure()
    }
}

/// MARK: - DEVN+Connection
public extension DEVN {
    /// Creates connection between current DEVN and given DEVS
    ///
    /// - Parameters:
    ///   - devs: DEVS
    ///   - transformation: Transforming function
    /// - Returns: New connection
    func connect<D: DEVS>(to devs: D, transformation: @escaping (Y) -> D.X?) -> DEVNConnection {
        return Connection.DevnToDevs<Self, D>(from: self, to: devs, transformation: transformation)
    }

    /// Creates connection between current DEVN and given DEVN
    ///
    /// - Parameters:
    ///   - devn: DEVN
    ///   - transformation: Transforming function
    /// - Returns: New connection
    func connect<D: DEVN>(to devn: D, tranformation: @escaping (Y) -> D.X?) -> DEVNConnection {
        return Connection.DevnToDevn(from: self, to: devn, transformation: tranformation)
    }
}

/// MARK: - DEVN+makeCoordinableSystem
public extension DEVN {
    /// Creates Coordinator
    ///
    /// - Returns: DEVNCoordinator
    func makeCoordinableSystem() -> Coordinable {
        return Coordinator(devn: self)
    }
}

class DefaultDEVNWrapper<D: DEVN>: DEVNWrapper {

    var id: UUID {
        return devn.id
    }

    var devses: [DEV]
    private let influencers: [UUID: Set<UUID>]
    private let influencedBy: [UUID: Set<HashableDev>]

    private let transformations: [UUID: [UUID: DEVSTransformation]]

    private let devn: D

    init(wrapping devn: D) {
        self.devn = devn
        let connections = devn.connections
        precondition(!connections.isEmpty)
        self.devses = Set(
            connections.flatMap { [$0.destination, $0.source] }
                .compactMap { $0 }
                .map { HashableDev(dev: $0) }
            ).map { $0.dev }
        var inf = [UUID: Set<UUID>]()
        var infBy = [UUID: Set<HashableDev>]()
        var trans = [UUID: [UUID: DEVSTransformation]]()
        for con in connections {
            let from = con.source?.id ?? devn.id
            let to = con.destination?.id ?? devn.id
            precondition(from != to)
            var i1 = inf[to] ?? []
            i1.insert(from)
            inf[to] = i1
            if let toDev = con.destination {
                var i2 = infBy[from] ?? []
                i2.insert(HashableDev(dev: toDev))
                infBy[from] = i2
            }
            var tx = trans[from] ?? [:]
            tx[to] = con.transformation
            trans[from] = tx
        }
        transformations = trans
        influencers = inf
        influencedBy = infBy
    }

    func isInfluencer(_ dout: DEV, of din: DEV) -> Bool {
        return influencers[din.id]?.contains(dout.id) ?? false
    }
    func influenced(by dout: DEV) -> [DEV] {
        return influencedBy[dout.id]?.map { $0.dev } ?? []
    }

    func transform(output y: DEVSEvent, of dout: DEV, toInputOf din: DEV) -> DEVSEvent? {
        return transformations[dout.id]?[din.id]?(y)
    }

    func select(from devses: [DEV]) -> DEV {
        return devn.select(from: devses)
    }

    func makeCoordinableSystem() -> Coordinable {
        return Coordinator(devn: devn)
    }
}
