//
//  DEVS.swift
//  DEVS
//
//  Created by Filip Klembara on 14/06/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

/// DEVS = (X, Y, S, s_0, delta_ext, delta_int, lambda, ta)
/// X = {x | x is input}
/// Y = {y | y is output}
/// S = {s | s is inner state}
/// s_0 \in S = initial state
/// delta_ext: Q x X -> S = external function where Q = {(s, e) | s \in S, 0 <= e <= ta(s)}
/// delta_int: S -> S = internal function
/// lamda: S -> Y = output function
/// ta: S -> R^+ \union {\infinity} = timer function returning time until next internal event
public protocol DEVS: DEV {
    /// Input
    associatedtype X: DEVSEvent
    /// Output
    associatedtype Y: DEVSEvent
    /// State
    associatedtype S: DEVSState

    /// Initial state
    var initialState: S { get }

    /// Do not use (only for internal usage)
    var currentStateClosure: (() -> S)? { get set }

    func deltaExt(state: S, e: DEVSTime, x: X) -> S
    func deltaInt(state: S) -> S
    func lambda(state: S) -> Y?
    func ta(state: S) -> DEVSTime
}

/// MARK: - DEVS+makeCoordinableSystem + current state
public extension DEVS {
    /// Returns current state
    ///
    /// - Note: This drops app if DEVS is not simulatating
    var currentState: S {
        // swiftlint:disable:next force_unwrapping
        return currentStateClosure!()
    }

    /// Creates coordinable system
    ///
    /// - Returns: Simulator
    func makeCoordinableSystem() -> Coordinable {
        return Simulator(devs: self)
    }
}

/// MARK: - DEVS+connections
public extension DEVS {
    /// Connect current DEVS to given DEVS
    ///
    /// - Parameters:
    ///   - devs: DEVS
    ///   - transformation: Transforming function
    /// - Returns: New connection
    func connect<D: DEVS>(to devs: D, transformation: @escaping (Y) -> D.X?) -> DEVNConnection {
        return Connection.DevsToDevs<Self, D>(from: self, to: devs, transformation: transformation)
    }
    /// Connect current DEVS to given DEVS
    ///
    /// - Parameters:
    ///   - devs: DEVS
    /// - Returns: New connection
    func connect<D: DEVS>(to devs: D) -> DEVNConnection where Y == D.X {
        return Connection.DevsToDevs<Self, D>(from: self, to: devs) { $0 }
    }
    /// Connect current DEVS to given DEVN
    ///
    /// - Parameters:
    ///   - devn: DEVN
    ///   - transformation: Transforming function
    /// - Returns: New connection
    func connect<D: DEVN>(to devn: D, tranformation: @escaping (Y) -> D.X?) -> DEVNConnection {
        return Connection.DevsToDevn(from: self, to: devn, transformation: tranformation)
    }

    /// Connect current DEVS to given DEVN
    ///
    /// - Parameters:
    ///   - devn: DEVN
    /// - Returns: New connection
    func connect<D: DEVN>(to devn: D) -> DEVNConnection where Y == D.X {
        return Connection.DevsToDevn(from: self, to: devn) { $0 }
    }
}
