//
//  Connection.swift
//  DEVS
//
//  Created by Filip Klembara on 16/06/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

/// Connection protocol
public protocol DEVNConnection {
    /// Source
    var source: DEV? { get }
    /// Destination
    var destination: DEV? { get }
    /// Tranformating function
    var transformation: DEVSTransformation { get }
}

/// Connection
public enum Connection {
    /// Devs to Devs connection
    public struct DevsToDevs<I: DEVS, D: DEVS>: DEVNConnection {
        /// Source
        public let source: DEV?
        /// Destination
        public let destination: DEV?
        /// Transforming function
        public let transformation: DEVSTransformation

        /// Init new connection
        ///
        /// - Parameters:
        ///   - source: Source
        ///   - destination: Destination
        ///   - transformation: Transforming function
        public init(from source: I, to destination: D, transformation: @escaping (I.Y) -> D.X?) {
            self.source = source
            self.destination = destination
            // swiftlint:disable:next force_cast
            self.transformation = { transformation($0 as! I.Y) }
        }
    }

    public struct DevsToDevn<I: DEVS, D: DEVN>: DEVNConnection {
        /// Source
        public let source: DEV?
        /// Destination
        public let destination: DEV?
        /// Transforming function
        public let transformation: DEVSTransformation

        /// Init new connection
        ///
        /// - Parameters:
        ///   - source: Source
        ///   - destination: Destination
        ///   - transformation: Transforming function
        public init(from source: I, to destination: D, transformation: @escaping (I.Y) -> D.X?) {
            self.source = source
            self.destination = destination
            // swiftlint:disable:next force_cast
            self.transformation = { transformation($0 as! I.Y) }
        }
    }

    public struct DevnToDevs<I: DEVN, D: DEVS>: DEVNConnection {
        /// Source
        public let source: DEV?
        /// Destination
        public let destination: DEV?
        /// Transforming function
        public let transformation: DEVSTransformation

        /// Init new connection
        ///
        /// - Parameters:
        ///   - source: Source
        ///   - destination: Destination
        ///   - transformation: Transforming function
        public init(from source: I, to destination: D, transformation: @escaping (I.Y) -> D.X?) {
            self.source = source
            self.destination = destination
            // swiftlint:disable:next force_cast
            self.transformation = { transformation($0 as! I.Y) }
        }
    }

    public struct DevnToDevn<I: DEVN, D: DEVN>: DEVNConnection {
        /// Source
        public let source: DEV?
        /// Destination
        public let destination: DEV?
        /// Transforming function
        public let transformation: DEVSTransformation

        /// Init new connection
        ///
        /// - Parameters:
        ///   - source: Source
        ///   - destination: Destination
        ///   - transformation: Transforming function
        public init(from source: I, to destination: D, transformation: @escaping (I.Y) -> D.X?) {
            self.source = source
            self.destination = destination
            // swiftlint:disable:next force_cast
            self.transformation = { transformation($0 as! I.Y) }
        }
    }

    public struct InDevs<I: DEVN, D: DEVS>: DEVNConnection {
        /// Source
        public let source: DEV? = nil
        /// Destination
        public let destination: DEV?
        /// Transforming function
        public let transformation: DEVSTransformation

        /// Init new connection
        ///
        /// - Parameters:
        ///   - destination: Destination
        ///   - transformation: Transforming function
        public init(to destination: D, transformation: @escaping (I.X) -> D.X?) {
            self.destination = destination
            // swiftlint:disable:next force_cast
            self.transformation = { transformation($0 as! I.X) }
        }
    }

    public struct InDevn<I: DEVN, D: DEVN>: DEVNConnection {
        /// Source
        public let source: DEV? = nil
        /// Destination
        public let destination: DEV?
        /// Transforming function
        public let transformation: DEVSTransformation

        /// Init new connection
        ///
        /// - Parameters:
        ///   - destination: Destination
        ///   - transformation: Transforming function
        public init(to destination: D, transformation: @escaping (I.X) -> D.X?) {
            self.destination = destination
            // swiftlint:disable:next force_cast
            self.transformation = { transformation($0 as! I.X) }
        }
    }

    public struct OutDevs<I: DEVS, D: DEVN>: DEVNConnection {
        /// Source
        public let source: DEV?
        /// Destination
        public let destination: DEV? = nil
        /// Transforming function
        public let transformation: DEVSTransformation

        /// Init new connection
        ///
        /// - Parameters:
        ///   - destination: Destination
        ///   - transformation: Transforming function
        public init(from source: I, transformation: @escaping (I.Y) -> D.Y?) {
            self.source = source
            // swiftlint:disable:next force_cast
            self.transformation = { transformation($0 as! I.Y) }
        }
    }

    public struct OutDevn<I: DEVN, D: DEVN>: DEVNConnection {
        /// Source
        public let source: DEV?
        /// Destination
        public let destination: DEV? = nil
        /// Transforming function
        public let transformation: DEVSTransformation

        /// Init new connection
        ///
        /// - Parameters:
        ///   - destination: Destination
        ///   - transformation: Transforming function
        public init(from source: I, transformation: @escaping (I.Y) -> D.Y?) {
            self.source = source
            // swiftlint:disable:next force_cast
            self.transformation = { transformation($0 as! I.Y) }
        }
    }
}
