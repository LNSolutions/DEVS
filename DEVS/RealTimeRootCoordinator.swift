//
//  RealTimeRootCoordinator.swift
//  DEVS
//
//  Created by Filip Klembara on 16/06/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

let devDoubleComparationPrecise = 6

/// Real time coordinator
public class RealTimeRootCoordinator<X: DEVSEvent> {
    private let system: Coordinable
    private let output: (DEVSEvent) -> Void

    /// True if simulation is running
    public private(set) var isRunning = false
    private let mutex = DispatchSemaphore(value: 1)

    /// Init coordinator
    ///
    /// - Parameters:
    ///   - model: DEV model
    ///   - output: Output function
    ///   - y: Output
    public init<T: DEVSEvent>(model: DEV, output: @escaping (T) -> Void) {
        system = model.makeCoordinableSystem()
        self.output = { event in
            // swiftlint:disable:next force_cast
            let e = event as! T
            output(e)
        }
    }

    /// Current time
    public var currentTime: Double {
        return system.tl + (Date().timeIntervalSince(tl))
    }

    private var t = Double.infinity

    private var t0 = Double.infinity

    private var tl = Date()

    private var timer: Timer!

    private func plannedMessage() {
        if let event = system.plannedMessage(t: t) {
            output(event)
        }
    }

    private func callAllPlaned() {
        var tn = system.tn
        while Double.equal(tn, t, precise: devDoubleComparationPrecise) {
            plannedMessage()
            tn = system.tn
        }
    }

    private func next() {
        callAllPlaned()
        let tn = system.tn
        guard tn < Double.infinity else {
            timer.fireDate = Date() + Double.infinity
            return
        }
        let e = tn - t
        tl = Date()
        t = tn
        timer.fireDate = tl + e
    }

    /// Advance time
    ///
    /// - Parameter newTime: New time
    public func advanceTime(to newTime: Double) {
        mutex.wait()
        defer {
            mutex.signal()
        }
        timer.fireDate = Date() + .infinity
        if newTime < system.tl {
            let e = t - newTime
            timer.fireDate = tl + e
        } else if newTime > t {
            while t < newTime {
                callAllPlaned()
                t = system.tn
            }
            tl = Date() - (newTime - system.tl)
            timer.fireDate = Date() + (t - newTime)
        } else {
            let d = newTime - system.tl
            tl = Date() - d
            let e = t - newTime
            timer.fireDate = Date() + e
        }
    }

    /// Start simulation
    public func startSimulation() {
        mutex.wait()
        defer {
            mutex.signal()
        }
        isRunning = true
        t = 0
        t0 = t
        tl = Date()
        system.initialMessage(t: t)
        timer = Timer(timeInterval: Double.infinity, repeats: true) { [weak self] timer in
            guard let s = self else {
                timer.invalidate()
                return
            }
            s.mutex.wait()
            defer {
                s.mutex.signal()
            }

            s.next()
        }
        RunLoop.current.add(timer, forMode: .default)
        next()
    }

    /// Async simulation finish
    public func asyncFinishSimulation() {
        DispatchQueue.global(qos: .userInitiated).async {
            self.mutex.wait()
            defer { self.mutex.signal() }
            self.isRunning = false
            self.timer?.invalidate()
        }
    }

    /// Input message
    ///
    /// - Parameter x: Input
    public func inputMessage(x: X) {
        mutex.wait()
        defer {
            mutex.signal()
        }
        guard isRunning else {
            return
        }
        timer.fireDate = Date() + .infinity
        let diff = Date().timeIntervalSince(tl)
        t = max(min(t, system.tl + diff), system.tl)
        system.inputMessage(x: x, t: t)

        next()
    }

    /// deinit
    deinit {
        timer?.invalidate()
    }
}
