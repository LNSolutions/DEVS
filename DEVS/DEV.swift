//
//  DEV.swift
//  DEVS
//
//  Created by Filip Klembara on 16/06/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import Foundation

/// DEV protocol
public protocol DEV: AnyObject, CoordinableSystem {
    var id: UUID { get }
}

struct HashableDev {
    let dev: DEV
}

extension HashableDev: Hashable, Equatable {
    static func == (lhs: HashableDev, rhs: HashableDev) -> Bool {
        return lhs.dev.id == rhs.dev.id
    }

    func hash(into hasher: inout Hasher) {
        dev.id.hash(into: &hasher)
    }
}

/// Event
public protocol DEVSEvent { }

/// State
public protocol DEVSState { }

/// Transformation
public typealias DEVSTransformation = (DEVSEvent) -> DEVSEvent?

/// Time base
public typealias DEVSTime = Double
