//
//  Coordinable.swift
//  DEVS
//
//  Created by Filip Klembara on 16/06/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

/// Coordinable system
public protocol CoordinableSystem {
    /// Creates system
    ///
    /// - Returns: New Coordinable
    func makeCoordinableSystem() -> Coordinable
}

/// Coordinable
public protocol Coordinable {
    /// Initial message
    ///
    /// - Parameter t: Time
    func initialMessage(t: DEVSTime)

    /// Internal planned message
    ///
    /// - Parameter t: Time
    /// - Returns: Output
    func plannedMessage(t: DEVSTime) -> DEVSEvent?

    /// Input message
    ///
    /// - Parameters:
    ///   - x: Input
    ///   - t: time
    func inputMessage(x: DEVSEvent, t: DEVSTime)

    /// Next event time
    var tn: DEVSTime { get }

    /// Last event time
    var tl: DEVSTime { get }
}
