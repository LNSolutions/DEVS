# DEVS
Swift library for discrete event simulations

# Installation

If you are using Cocoapods add this to your *Podfile*

```ruby
    pod 'DEVS'
```
